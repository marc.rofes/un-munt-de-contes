from django.urls import path
from .views import Contacto, Escoles, Domicili

urlpatterns = [
    path('contacte', Contacto.as_view(), name='contacte'),
    path('serveis/escoles', Escoles.as_view(), name='escoles'),
    path('serveis/domicili', Domicili.as_view(), name='domicili'),
]