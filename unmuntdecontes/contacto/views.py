from django.shortcuts import render, redirect
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.views.generic import TemplateView
from .forms import correo
from django.http import HttpResponseRedirect



# Create your views here.
class Contacto(TemplateView):
    def get(self,request):
        contacto=correo()
        return render(request,'contacto/email.html',{'contacto':contacto})
    def post(self,request):
        contacto=correo(request.POST)
        if contacto.is_valid():
            asunto= 'Este es un mensaje de la pagina Un munt de contes'
            mensaje= contacto.cleaned_data['mensaje']
            mail= EmailMessage(asunto, mensaje, to=['martajuglares@gmail.com'])
            mail.send()
            return HttpResponseRedirect('/')
        return render(request,'contacto/confirmat.html',{'contacto':contacto})

class Escoles(TemplateView):
    def get(self,request):
        contacto=correo()
        return render(request,'portal/serveis/escoles.html',{'contacto':contacto})
    def post(self,request):
        contacto=correo(request.POST)
        if contacto.is_valid():
            asunto= 'Una Escuela Quiere Contactarte'
            mensaje= contacto.cleaned_data['mensaje']
            mail= EmailMessage(asunto, mensaje, to=['martajuglares@gmail.com'])
            mail.send()
            return HttpResponseRedirect('/')
        return render(request,'contacto/confirmat.html',{'contacto':contacto})

class Domicili(TemplateView):
    def get(self,request):
        contacto=correo()
        return render(request,'portal/serveis/domicili.html',{'contacto':contacto})
    def post(self,request):
        contacto=correo(request.POST)
        if contacto.is_valid():
            asunto= 'Un Domicilio Quiere Contactarte'
            mensaje= contacto.cleaned_data['mensaje']
            mail= EmailMessage(asunto, mensaje, to=['martajuglares@gmail.com'])
            mail.send()
            return HttpResponseRedirect('/')
        return render(request,'contacto/confirmat.html',{'contacto':contacto})