from django.shortcuts import render, redirect

# Create your views here.
def inici(request):
    return render(request, 'portal/inici.html', {})

def quisom(request):
    return render(request, 'portal/qui-som.html', {})

def serveis(request):
    return render(request, 'portal/serveis.html', {})