from django.urls import path
from . import views

urlpatterns = [
    path('', views.inici, name='inici'),
    path('qui-som', views.quisom, name='qui-som'),
    path('serveis', views.serveis, name='serveis'),
]