from django.urls import path
from . import views

urlpatterns = [
    path('inici', views.inicisessio, name='inicisessio'),
    path('categories', views.categories, name='categories'),
    path('<slug>', views.pcategorias, name='categoria'),
    path('<slug>/<slug_contes>', views.contes, name='contes'),
]