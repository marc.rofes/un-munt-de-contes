from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.shortcuts import get_object_or_404
from .models import Categorias, Contes

# Create your views here.

def inicisessio(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request, 'contes/inici.html', {})
    # En otro caso redireccionamos al login
    return redirect('/iniciar-sessio')

def categories(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        categorias = Categorias.objects.all()
        return render(request, 'contes/categories.html', {'categorias':categorias})
    # En otro caso redireccionamos al login
    return redirect('/iniciar-sessio')

def pcategorias(request, slug):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        categorias = get_object_or_404(Categorias, slug=slug)
        contes_lista = Contes.objects.filter(categorias__slug=slug).order_by('-id')
        page = request.GET.get('page', 1)
        paginator = Paginator(contes_lista, 9)
        try:
            contes = paginator.page(page)
        except PageNotAnInteger:
            contes = paginator.page(1)
        except EmptyPage:
            contes = paginator.page(paginator.num_pages)
        contexto = {
            'categorias':categorias, 
            'contes':contes,
        }
        return render(request, 'contes/mycategory_detail.html', contexto)
    # En otro caso redireccionamos al login
    return redirect('/iniciar-sessio')
    
def contes(request, slug, slug_contes):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        categorias = get_object_or_404(Categorias, slug=slug)
        conte = get_object_or_404(Contes, slug_contes=slug_contes)
        contexto = {
            'categorias' : categorias,
            'conte': conte,
        }
        return render(request, 'contes/conte.html', contexto)
    # En otro caso redireccionamos al login
    return redirect('/iniciar-sessio')
    
