from django.db import models
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from django.urls import reverse

# Create your models here.
class Categorias(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=50)
    color = models.CharField(max_length=10)
    slug = models.CharField(max_length = 150, unique = True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"
    
    def get_absolute_url(self):
        return reverse('Categoria', kwargs={'slug': self.slug})

class Contes(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=70)
    texto = RichTextField()
    categorias = models.ForeignKey(Categorias, on_delete=models.CASCADE)
    fecha = models.DateTimeField()
    imagen = models.ImageField(upload_to ='')
    slug_contes = models.SlugField()

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Conte"
        verbose_name_plural = "Contes"

