from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.welcome),
    path('registrar', views.register, name="registrar"),
    path('iniciar-sessio', views.login, name="iniciar-sessio"),
    path('logout', views.logout, name="logout"),
]